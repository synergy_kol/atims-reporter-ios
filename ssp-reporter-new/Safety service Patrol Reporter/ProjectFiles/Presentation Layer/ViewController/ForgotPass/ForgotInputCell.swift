//
//  ForgotInputCell.swift
//  Safety service Patrol Reporter
//
//  Created by Mahesh Mahalik on 07/05/20.
//  Copyright © 2020 met. All rights reserved.
//

import UIKit

class ForgotInputCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtInputField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
