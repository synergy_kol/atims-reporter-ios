//
//  ShiftTypeCell.swift
//  Safety service Patrol Reporter
//
//  Created by Dipika on 14/02/20.
//  Copyright © 2020 met. All rights reserved.
//

import UIKit

class ShiftTypeCell: UITableViewCell {
    
    @IBOutlet weak var labelCityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
