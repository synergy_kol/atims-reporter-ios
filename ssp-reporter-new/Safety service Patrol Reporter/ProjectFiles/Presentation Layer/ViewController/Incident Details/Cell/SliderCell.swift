//
//  SliderCell.swift
//  Alkindi
//
//  Created by Mahesh Mahalik on 30/01/20.
//  Copyright © 2020 Alkindi. All rights reserved.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewSlide: UIImageView!
}
