//
//  AddCrashCommentCell.swift
//  Safety service Patrol Reporter
//
//  Created by Mahesh Mahalik on 08/04/20.
//  Copyright © 2020 met. All rights reserved.
//

import UIKit

class AddCrashCommentCell: UITableViewCell {
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var btnAddThird: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
