//
//  DashboardCollCell.swift
//  Safety service Patrol Reporter
//
//  Created by Dipika on 11/02/20.
//  Copyright © 2020 met. All rights reserved.
//

import UIKit

class DashboardCollCell: UICollectionViewCell {
     @IBOutlet weak var lblNew: UILabel!
    @IBOutlet weak var imgDashboard: UIImageView!
    @IBOutlet weak var labelDashboard: UILabel!
}
